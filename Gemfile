source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.0'

gem 'audited', '~> 5.0'
gem 'aws-sdk-s3'
gem 'bootsnap', require: false
gem 'boxing'
gem 'cancancan'
gem 'cssbundling-rails'
gem 'coffee-rails'
gem 'devise'
gem 'enum_help'
gem 'friendly_id'
gem 'image_processing', '~> 1.2'
gem 'jbuilder'
gem 'jsbundling-rails'
gem 'name_of_person'
gem 'paper_trail'
gem 'paranoia'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'rails', '~> 7.0.1'
gem 'redis', '~> 4.0'
gem 'sassc-rails'
gem 'simple_form'
gem 'slim-rails'
gem 'sprockets-rails'
gem 'stimulus-rails'
gem 'turbo-rails'
gem 'stripe', '~> 5.39' # Ruby library for the Stripe API
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'roo'
gem 'roo-xls'
gem 'wicked'
gem 'noticed'
gem 'money'

group :development, :test do
  gem 'annotate'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  # A library for generating fake data such as names, addresses, and phone numbers
  gem 'faker', '~> 2.19'
  gem 'rails-controller-testing'
  # rspec-rails is a testing framework for Rails 3+
  gem 'rspec-rails', '~> 5.0'
  # Automatic Ruby code style checking tool
  gem 'rubocop', '~> 1.22', require: false
  gem 'rubocop-gitlab-security'
  # A collection of RuboCop cops to check for performance optimizations in Ruby code
  gem 'rubocop-performance', '~> 1.11', require: false
  # Automatic Rails code style checking tool
  gem 'rubocop-rails', '~> 2.12', require: false
  # Code style checking for RSpec files
  gem 'rubocop-rspec', '~> 2.5', require: false
end

group :development do
  # Provides a better error page for Rails and other Rack apps
  gem 'better_errors', '~> 2.9'
  gem 'binding_of_caller'
  # Brakeman detects security vulnerabilities in Ruby on Rails applications via static analysis
  gem 'brakeman', '~> 5.1', require: false
  # bundler-audit provides patch-level verification for Bundled apps
  gem 'bundler-audit', '~> 0.9'
  # Guard::LiveReload automatically reloads your browser when 'view' files are modified
  gem 'guard-livereload', '~> 2.5', require: false
  gem 'hotwire-livereload'
  # Helpers to find and manage missing and unused translations
  gem 'i18n-tasks', '~> 0.9.34'
  gem 'letter_opener_web'
  gem 'pry-rails'
  gem 'rack-mini-profiler'
  gem 'spring'
  gem 'web-console'
end

group :test do
  # factory_bot is a fixtures replacement with a straightforward definition syntax, support for multiple build strategies
  gem 'factory_bot_rails', '~> 6.2'
  gem 'fuubar'
  gem 'selenium-webdriver'
  # Simple one-liner tests for common Rails functionality
  gem 'shoulda-matchers', '~> 5.0.0', require: false
  # Code coverage with a powerful configuration library and automatic merging of coverage across test suites
  gem 'simplecov', '~> 0.21.2', require: false
  # Run Selenium tests more easily with install and updates for all supported webdrivers
  gem 'webdrivers'
end
