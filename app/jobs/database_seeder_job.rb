class DatabaseSeederJob < ApplicationJob
  queue_as :default

  DEFAULT_USER_USERNAME = 'Administrador'
  DEFAULT_USER_EMAIL = 'administrador@example.com'
  DEFAULT_USER_PASSWORD = 'administrador'
  DEFAULT_USER_ADMIN = true

  def perform
    ActiveRecord::Base.transaction do
      create_roles
      create_default_user
    end
  end

  private

  def create_roles
    Role.create!(code: 'super', name: 'Super Administrador')
    Role.create!(code: 'admin', name: 'Administrador')
    Role.create!(code: 'user', name: 'Usuario')
  end

  def create_default_user
    role = Role.find_by(code: 'super')
    User.create!(
      username: DEFAULT_USER_USERNAME,
      email: DEFAULT_USER_EMAIL,
      password: DEFAULT_USER_PASSWORD,
      password_confirmation: DEFAULT_USER_PASSWORD,
      role_id: role.id,
      admin?: DEFAULT_USER_ADMIN
    )
  end
end
