# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_roles_on_deleted_at  (deleted_at)
#  index_roles_on_slug        (slug)
#

class Role < ApplicationRecord
  has_one :user, inverse_of: :role
end
